package org.acme.repository;


import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.model.Tag;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TagRepository implements PanacheRepository<Tag> {

    @Override
    public Tag findById(Long id) {
        return find("id", id).firstResult();
    }
}
