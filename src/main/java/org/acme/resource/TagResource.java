package org.acme.resource;

import org.acme.domain.TagReq;
import org.acme.model.Post;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Set;

@Path("/tag")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagResource {

    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;


    @POST
    @Transactional
    public String save(TagReq tagReq){

        Tag tag = new Tag();
        tag.setLabel(tagReq.getLabel());

        Post post = postRepository.findById(Long.valueOf(tagReq.getPostId()));
        Set<Post> posts = new HashSet<>();
        posts.add(post);

        tag.setPosts(posts);
        tagRepository.persist(tag);
        return "Success";
    }

    @GET
    @Path("{id}")
    public Tag get(@PathParam("id") long id)
    {
        return tagRepository.findById(id);
    }

    @PUT
    @Transactional
    @Path("{id}")
    public String update(@PathParam("id") long id, TagReq tagReq)
    {
        Tag tag = tagRepository.findById(id);
        tag.setLabel(tagReq.getLabel());

        Post post = postRepository.findById(Long.valueOf(tagReq.getPostId()));
        Set<Post> posts = new HashSet<>();
        posts.add(post);

        tag.setPosts(posts);

        tagRepository.persist(tag);
        return "Success";
    }

    @DELETE
    @Transactional
    @Path("{id}")
    public String delete(@PathParam("id") long id)
    {
        tagRepository.deleteById(id);
        return "Success";
    }
}
