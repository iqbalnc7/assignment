package org.acme.resource;

import org.acme.domain.PostReq;
import org.acme.model.Post;
import org.acme.model.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Set;

@Path("/post")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResource {

    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;


    @POST
    @Transactional
    public String save(PostReq postReq){

        Post post = new Post();
        post.setContent(postReq.getContent());
        post.setTitle(postReq.getTitle());

        Tag tag = tagRepository.findById(Long.valueOf(postReq.getTagId()));
        Set<Tag> tags = new HashSet<Tag>();
        tags.add(tag);

        post.setTags(tags);
        postRepository.persist(post);
        return "Success";
    }

    @GET
    @Path("{id}")
    public Post get(@PathParam("id") long id)
    {
        return postRepository.findById(id);
    }

    @PUT
    @Transactional
    @Path("{id}")
    public String update(@PathParam("id") long id, PostReq postReq)
    {
        Post post = postRepository.findById(id);
        post.setTitle(postReq.getTitle());
        post.setContent(postReq.getContent());

        Tag tag = tagRepository.findById(Long.valueOf(postReq.getTagId()));
        Set<Tag> tags = new HashSet<Tag>();
        tags.add(tag);

        post.setTags(tags);

        postRepository.persist(post);
        return "Success";
    }

    @DELETE
    @Transactional
    @Path("{id}")
    public String delete(@PathParam("id") long id)
    {
        postRepository.deleteById(id);
        return "Success";
    }


}