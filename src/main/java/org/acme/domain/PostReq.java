package org.acme.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostReq {
    private String title;
    private String content;
    private String tagId;
}
