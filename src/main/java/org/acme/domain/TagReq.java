package org.acme.domain;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TagReq {
    private String label;
    private String postId;
}
