package org.acme.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "tag")
public class Tag {

    @Id
    @SequenceGenerator(name = "generator", sequenceName = "TAG_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "label")
    private String label;

    @ManyToMany(mappedBy = "tags")
    private Set<Post> posts;



}
